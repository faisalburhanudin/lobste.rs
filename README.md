# Tugas 1 (Individu)
Lobsters (https://lobste.rs/) adalah mesin agregasi tautan (link-aggregation) dengan fokus pada teknologi.

Pada tugas pertama ini, Anda tidak akan membuat program Lobsters melainkan Anda akan ditugaskan untuk mempelajari website Lobsters kemudian membuat daftar _endpoint_ yang dibutuhkan untuk membuat sistem Lobsters.

Masing-masing _endpoint_ memiliki dokumentasi yang lengkap dan ditulis menggunakan syntax markdown. Lihat contoh dokumentasi pada file doc_example.MD

## Todo
- /messages
- registrasi
- improv Error response
- upvote / downvote