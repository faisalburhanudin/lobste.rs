# Stories Details

Get stories details

## URL
/s

## Methods
GET

## Data Params
- **id** [string] id stories
- **title** [string] title stories *optional*

Example:
*with title*
`/s/orlis8/unix_is_not_an_acceptable_unix`

*no title*
`/s/orlis8/`

## Success Response
This method will return stories details.
```json
HTTP/1.1 200 Success
{
    "stories": {
        "id": "story_orlis8",
        "voters": {
            "score": 31,
        },
        "details": {
            "link": {
                "url": "http://mkremins.github.io/blog/unix-not-acceptable-unix/",
                "title": "Unix is not an acceptable Unix",
            },
            "tag": [
                {
                    "url": "/t/practices",
                    "title": "practices",
                    "details": "Development and business practices",
                },
                {
                    "url": "/t/programming",
                    "title": "programming",
                    "details": "General software development",
                }
            ],
            "domain": {
                "url": "/search?q=domain:blog.jessitron.com&order=newest",
                "title": "blog.jessitron.com",
            },
            "by": {
                "user": {
                    "url": "/u/pushcx",
                    "name": "pushcx",
                },
                "posted_in" : "2015-06-07 10:21:02 -0500"
            },
            "comment_count": 4

        },
    },
    "comments": [
        {
            "id": "comment_asbbec",
            "voters": {
                "count": 5,
            },
            "details": {
                "user": {
                    "url": "/u/bitemyapp",
                    "name": "bitemyapp",
                },
                "posted_in" : "2015-06-07 10:21:02 -0500",
                "text": "For more depth and specificity, see Why free monads matter. Free monads are not the only way to separate IO and computation, but it’s one relatively popular way. They lend themselves nicely to making “test” interpreters of your data model that mock effects but don’t actually run them. Example: The production interpreter performs IO, the fake/test/mock one logs effects performed via a Writer and your tests can assert and check things like, “it wrote ${content} three times to ${file}” without having to check the secondary outcomes of having performed said effects. Your tests can be a lot more precise and more realistic without adding actual effects to the test suite. Have to be judicious in your assertions still - you don’t want to be updating tedious assertions every time something changes. IME: Having types that track effects period is vital for making this work outside of small demo projects. Not just for my own code, but so I know what other peoples' code is doing as well. You know how people individually (painfully) discover how Java’s URL equality works? You don’t get nasty surprises like that in Haskell. You can write code that performs the same check, but you don’t get to abuse Eq for it.",
            },
            "comments": [
                {
                    "id": "comment_asbbec",
                    "voters": {
                        "count": 5,
                    },
                    "details": {
                        "user": {
                            "url": "/u/bitemyapp",
                            "name": "bitemyapp",
                        },
                        "posted_in" : "2015-06-07 10:21:02 -0500",
                        "text": "For more depth and specificity, see Why free monads matter. Free monads are not the only way to separate IO and computation, but it’s one relatively popular way. They lend themselves nicely to making “test” interpreters of your data model that mock effects but don’t actually run them. Example: The production interpreter performs IO, the fake/test/mock one logs effects performed via a Writer and your tests can assert and check things like, “it wrote ${content} three times to ${file}” without having to check the secondary outcomes of having performed said effects. Your tests can be a lot more precise and more realistic without adding actual effects to the test suite. Have to be judicious in your assertions still - you don’t want to be updating tedious assertions every time something changes. IME: Having types that track effects period is vital for making this work outside of small demo projects. Not just for my own code, but so I know what other peoples' code is doing as well. You know how people individually (painfully) discover how Java’s URL equality works? You don’t get nasty surprises like that in Haskell. You can write code that performs the same check, but you don’t get to abuse Eq for it.",
                    },
                    "comments": [
                        
                    ]
                }
            ]
        }
    ]
}
```

## Error Response
