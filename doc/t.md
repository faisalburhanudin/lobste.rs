# Stories by tag

List of stories by tag.

## URL
/t

## Methods
GET

## Data Params
- **nametag** [string] tag name

Example:
`/t/javascript`

## Success Response
This method will return submitted stories by tag.
```json
HTTP/1.1 200 Success
[
    {
        "id": "story_klvijs",

        "voters": {
            "score": 1
        },

        "details": {

            "link": {
                "url": "http://underscore.io/blog/posts/2015/06/02/everything-about-sealed.html",
                "title": "verything You Ever Wanted to Know About Sealed Traits in Scala",
            },

            "tags": [
                {
                    "url": "/t/scala",
                    "tag": "scala",
                    "title": "Scala programming",
                },
                {
                    "url": "/t/programming",
                    "tag": "programming",
                    "title": "General software development",
                },
            ],

            "user": {
                "url": "/u/SeanTAllen",
                "name": "SeanTAllen",
            },

            "timestamps": "2015-06-04 11:20:55 -0500",

            "comments": {
                "url": "/s/bpqehw/everything_you_ever_wanted_to_know_about_sealed_traits_in_scala",
                "count": 0,
            }
        },
    },
]
```

## Error Response
