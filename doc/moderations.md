# Recently Moderations Log

The newest recently moderations log.

## URL
/moderations

## Methods
GET

## Data Params
Not need paramenters, just `/moderations`

Example:
`/moderations`

## Success Response
This method will return recently moderations log
```json
HTTP/1.1 200 Success
[
    {
        "timestamps": "2015-06-04 09:51:39",

        'moderator_id': "jcs"

        "stories": {
            "url": "/s/bnigao/mac_attack_nasty_bug_lets_hackers_into_apple_computers",
            "title": "Story: Mac attack! Nasty bug lets hackers into Apple computers",
        }

        "action": "merged into wrpjek (How your Mac firmware security is completely broken)",

        "reason": "Click-bait headline, already posted anyway", 
    },
]
```

## Error Response
