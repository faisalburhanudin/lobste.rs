# User Details

Get user details

## URL
/u

## Methods
GET

## Data Params
- **userid** [string] tag name

Example:
`/u/pushcx`

## Success Response
This method will return user details.
```json
HTTP/1.1 200 Success
[
    {
        "username": "pushcx",

        "gravatar": "https://secure.gravatar.com/avatar/e7be134a6de3d308adf765be1004c450?r=pg&d=identicon&s=100",

        "status": "active user",

        "joined": "2012-08-14 20:25:08 -0500",

        "invitatin_by": "jcs",

        "karma": {
            "count": 6371, 
            "averaging": 6.04,
        },

        "stories_submitted": {
            "count": 874,
            "most_tag": "programming",
        },

        "comment_posted": 180,

        "about": "Lifetime hacker, Mathematician and happy Maker :)",
    },
]
```

## Error Response
