# Set Hide Content In Homepage

To hide stories from the home page that have been tagged with certain tags, if user not login filters will be stored in a long-lasting browser cookie.

## URL
/filters

## Methods
POST

## Data Params
- **tags** [array] tag name

Example:
```
{
    "tags": ["scala", "linux", "python"],
}
```

## Success Response
if success return success message

```json
HTTP/1.1 200 Success
{
    message: "Filters has been save"
}
```

## Error Response

# Get Hide Content In Homepage

To get hide stories has been set before.

## URL
/filters

## Methods
GET

## Data Params
Not need parameters, just `/filters`

Example:
`/filters`

## Success Response
if success return list filters who hide in homepage

```json
HTTP/1.1 200 Success
[
    {
        "tag": "android",
        "description": "Android",
        "stories": 186
    },
    {
        "tag": "api",
        "description": "API development/implementation",
        "stories": 199
    },
]
```

## Error Response