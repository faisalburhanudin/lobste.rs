# Get Recently Stories

The newest recently submitted stories that have not yet reached the front page.

## URL
/recent
/newest (alias)

## Methods
GET

## Data Params
Not need paramenters, just `/recent`

Example:
`/recent`

## Success Response
This method will return data `json` recently submitted stories
```json
HTTP/1.1 200 Success
[
    {
        "id": "story_klvijs",

        "voters": {
            "score": 1
        },

        "details": {

            "link": {
                "url": "http://underscore.io/blog/posts/2015/06/02/everything-about-sealed.html",
                "title": "verything You Ever Wanted to Know About Sealed Traits in Scala",
            },

            "tags": [
                {
                    "url": "/t/scala",
                    "tag": "scala",
                    "title": "Scala programming",
                },
                {
                    "url": "/t/programming",
                    "tag": "programming",
                    "title": "General software development",
                },
            ],

            "user": {
                "url": "/u/SeanTAllen",
                "name": "SeanTAllen",
            },

            "timestamps": "2015-06-04 11:20:55 -0500",

            "comments": {
                "url": "/s/bpqehw/everything_you_ever_wanted_to_know_about_sealed_traits_in_scala",
                "count": 0,
            }
        },
    },
]
```

## Error Response
