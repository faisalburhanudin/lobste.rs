# Get Recently Comments

The newest comments.

## URL
/comments

## Methods
GET

## Data Params
Not need paramenters, just `/recent`

Example:
`/comments`

## Success Response
This method will return data `json` recently comments
```json
[
    {
        "id": "comment_70qhhc",

        "voters": {
            "score": 2,
        },

        "details": {

            "attributes": {

                "name": "c_70qhhc",

                "user": {
                    "url": "/u/SeanTAllen",
                    "name": "SeanTAllen",
                },

                "edited": "True",

                "timestamps": "2015-06-07 04:02:57 -0500",

                "url": "https://lobste.rs/s/tel2ko/cockroach_labs_hello_world/comments/70qhhc#c_70qhhc",

                "story": {
                    "url": "/s/tel2ko/cockroach_labs_hello_world",
                    "title": "Cockroach Labs: Hello World",
                },
            },

            "comment": "What do you put in your marketing material?  This is not a design specification, and it serves a very different purpose as well as a much larger audience.  I think when your infrastructure is chosen based solely on this kind of material you have more serious issues ahead of you.  People who understand this issue also don&rsquo;t put important non-recomputable data into anything less than 5 years old and already proven (sometimes by them) at scale.  That said, I think a database with these characteristics actually can serve a majority of existing use-cases, so I&rsquo;m hoping to use it pretty extensively sometime down the line.",
        }
    }
]
```

## Error Response