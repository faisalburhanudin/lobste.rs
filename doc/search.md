# Search - submitted stories

return submitted stories by keyword

## URL
/search

## Methods
GET

## Data Params
- **q** [String] Search keyword
- **what** [Enumerate] What type of search. If not set, 'all' search is used.
    + all
    + stories
    + comments
- **order** [Enumerate] Sort order, default is 'relevance'
    + relevance
    + newest
    + points  

Example:
`/search?q=linux&what=all&order=relevance`

## Success Response
if success return list of search

### **what** [all,stories]
```json
HTTP/1.1 200 Success
[
    {
        "id": "story_klvijs",

        "voters": {
            "score": 1
        },

        "details": {

            "link": {
                "url": "http://underscore.io/blog/posts/2015/06/02/everything-about-sealed.html",
                "title": "verything You Ever Wanted to Know About Sealed Traits in Scala",
            },

            "tags": [
                {
                    "url": "/t/scala",
                    "tag": "scala",
                    "title": "Scala programming",
                },
                {
                    "url": "/t/programming",
                    "tag": "programming",
                    "title": "General software development",
                },
            ],

            "user": {
                "url": "/u/SeanTAllen",
                "name": "SeanTAllen",
            },

            "timestamps": "2015-06-04 11:20:55 -0500",

            "comments": {
                "url": "/s/bpqehw/everything_you_ever_wanted_to_know_about_sealed_traits_in_scala",
                "count": 0,
            }
        },
    },
]
```

### **what** [comments]
```json
HTTP/1.1 200 Success
[
    {
        "id": "comment_70qhhc",

        "voters": {
            "score": 2,
        },

        "details": {

            "attributes": {

                "name": "c_70qhhc",

                "user": {
                    "url": "/u/SeanTAllen",
                    "name": "SeanTAllen",
                },

                "edited": "True",

                "timestamps": "2015-06-07 04:02:57 -0500",

                "url": "https://lobste.rs/s/tel2ko/cockroach_labs_hello_world/comments/70qhhc#c_70qhhc",

                "story": {
                    "url": "/s/tel2ko/cockroach_labs_hello_world",
                    "title": "Cockroach Labs: Hello World",
                },
            },

            "comment": "What do you put in your marketing material?  This is not a design specification, and it serves a very different purpose as well as a much larger audience.  I think when your infrastructure is chosen based solely on this kind of material you have more serious issues ahead of you.  People who understand this issue also don&rsquo;t put important non-recomputable data into anything less than 5 years old and already proven (sometimes by them) at scale.  That said, I think a database with these characteristics actually can serve a majority of existing use-cases, so I&rsquo;m hoping to use it pretty extensively sometime down the line.",
        }
    }
]
```
## Error Response