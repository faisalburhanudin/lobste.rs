# Login user

login user method

## URL
/login

## Methods
POST

## Data Params 
- **email** [string] email or username
- **password**[string] user password

Example:
```
{
    "email": "faisal@google.com",
    "password": "hcutc73bc87g38"
}
```

## Success Response
if login success return success message

```json
HTTP/1.1 200 Success
{
    message: "Log in success"
}
```

## Error Response

# Forgot Password

recovery password

## URL
/login/forgot_password

## Methods
POST

## Data Params 
- **email** [string] user email

Example:
```
{
    "email": "faisal@google.com"
}
```

## Success Response
if email sendind success, return success message

```json
HTTP/1.1 200 Success
{
    message: "Email recovery has been send to your email"
}
```

## Error Response

